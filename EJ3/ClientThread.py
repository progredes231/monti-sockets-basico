import socket

HOST = "127.0.0.1"
PORT = 54321

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    print(f'Conectado a {HOST}:{PORT}')
    msg = input('Mensaje:')
    if msg != '':
        s.sendall(bytes(msg, encoding='ascii'))
        data = s.recv(1024)
        while data:
            if data.decode('utf-8') != 'exit':
                print(f'Mensaje recibido!', 'Servidor dice:', data.decode('utf-8'))
                msg = input('Mensaje:')
                s.sendall(bytes(msg, encoding='ascii'))
                data = s.recv(1024)
                if msg == 'exit':
                    break
            if data.decode('utf-8') == 'exit':
                print(f'Conexion finalizada por Servidor')
                break